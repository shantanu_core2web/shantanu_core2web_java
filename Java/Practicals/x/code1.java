


class Piramid{

	public static void main(String[] args){
	
		int row = 4 ;

		for(int i=1; i<=row; i++){
		
			for(int sp= row; sp>i; sp-- ){
			
				System.out.print("\t");
			}

			int num =1; 

			for(int j=1; j<=i*2-1; j++){
			
				if(j<i){

					System.out.print((num+=2)+"\t");
				}else{
				
					System.out.print((num-=2)+"\t");
				}


			}
			System.out.println(); 
		}
	}
}
