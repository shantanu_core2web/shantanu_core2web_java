
class UnaryOperator{
	public static void main(String[]args){
		int num1 = 14;
		int num2 = 22;

		System.out.println(++num1 + num2++);
		System.out.println(num1++ + ++num2 + ++num1 + ++num1);
		System.out.println(num2++ + ++num1 + ++num1);
	}
}

